package com.example.dumap.ui.main

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.io.ByteArrayOutputStream

class ViewModel : ViewModel() {
    var email = FirebaseAuth.getInstance().currentUser?.email
    var markersList = mutableListOf<Marker>()
    private val db = FirebaseFirestore.getInstance()

    var markerMap = 0
    var markerZoom = false


    fun loadMarks(){
        markersList.removeAll(markersList)
        db.collection("users/${email}/marks")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d("meh", "${document.id} => ${document.data.get("desc")}")
                    val id = document.id
                    val name = document.get("name")as String
                    val desc = document.get("desc")as String
                    val lat = document.get("lat")as Double
                    val lng = document.get("lng")as Double

                    val marker = Marker(id.toInt(), name, desc, lat.toFloat(), lng.toFloat())
                    markersList.add(marker)
                }
            }
    }

    fun getLastId(): Int{
        if(markersList.size == 0){
            return 0
        }else{
            return markersList.get(markersList.size-1).id+1
        }
    }
    fun getMarkers() = markersList


    fun search(search: String): List<Marker>{
        var searchChamps = mutableListOf<Marker>()

        for (i in markersList){
            if(i.name.contains(search, ignoreCase = true)){
                searchChamps.add(i)
            }
        }

        return searchChamps
    }


    fun toURI(context: Context, bitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
        return Uri.parse(path.toString())
    }
}