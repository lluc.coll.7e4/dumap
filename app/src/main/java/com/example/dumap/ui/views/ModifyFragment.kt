package com.example.dumap.ui.views

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.drawToBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dumap.R
import com.example.dumap.ui.main.ViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class ModifyFragment: Fragment(R.layout.modify_marker) {
    lateinit var markerName: TextInputEditText
    lateinit var markerDesc: TextInputEditText
    lateinit var camera: ImageView
    lateinit var delete: ImageView
    lateinit var mapIcon: ImageView
    lateinit var confirm: Button
    lateinit var cancel: Button

    var resultImg = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
            result ->
        if (result.resultCode == Activity.RESULT_OK){
            result.data.let { if (it != null){
                camera.setImageBitmap(it.extras!!.get("data") as Bitmap)
                imageChanged = true
            }
            }

        }
    }

    private val viewModel: ViewModel by activityViewModels()
    private val db = FirebaseFirestore.getInstance()

    var imageChanged = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        markerName = view.findViewById(R.id.markerName)
        markerDesc = view.findViewById(R.id.markerDesc)
        camera = view.findViewById(R.id.cameraImage)
        delete = view.findViewById(R.id.deleteMark)
        mapIcon = view.findViewById(R.id.mapIcon)
        confirm = view.findViewById(R.id.confirm_btn)
        cancel = view.findViewById(R.id.cancel_btn)

        val email = viewModel.email
        val position = arguments?.getInt("position")!!
        val mark = viewModel.getMarkers().get(position)

        markerName.setText(mark.name)
        markerDesc.setText(mark.desc)

        val storage = FirebaseStorage.getInstance().reference.child("images/${email}/${mark.id}")
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            camera.setImageBitmap(bitmap)

        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
                .show()
        }


        confirm.setOnClickListener{
            if (markerName.text!!.length != 0 && markerName.text.toString() != mark.name) {
                db.collection("users/${email}/marks").document(mark.id.toString()).update(
                    "name", markerName.text.toString()
                )
            }
            if(markerDesc.text!!.length != 0 && markerDesc.text.toString() != mark.desc) {
                db.collection("users/${email}/marks").document(mark.id.toString()).update(
                    "desc", markerDesc.text.toString()
                )
            }
            if(imageChanged){
                FirebaseStorage.getInstance().getReference("images/${email}/${mark.id}").putFile(viewModel.toURI(context!!, camera.drawToBitmap()))
                    .addOnSuccessListener {
                        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                    }
            }
            viewModel.loadMarks()
            val action = ModifyFragmentDirections.actionModifyFragmentToMarkersFragment()
            findNavController().navigate(action)
        }

        camera.setOnClickListener{
            val dialog = AlertDialog.Builder(context)
                .setTitle("Warning")
                .setMessage("Do you want to change the image?")
                .setNegativeButton("Cancel") { view, _ -> }
                .setPositiveButton("Continue") { view, _ ->
                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    try {
                        resultImg.launch(takePictureIntent)
                    } catch (e: ActivityNotFoundException) {
                    }
                }
                .setCancelable(false)
                .create()

            dialog.show()
        }

        cancel.setOnClickListener{
            val action = ModifyFragmentDirections.actionModifyFragmentToMarkersFragment()
            findNavController().navigate(action)
        }

        delete.setOnClickListener{
            val email = viewModel.email
            val dialog = AlertDialog.Builder(context)
                .setTitle("Warning")
                .setMessage("Do you want to delete this marker?")
                .setNegativeButton("Cancel") { view, _ -> }
                .setPositiveButton("Continue") { view, _ ->
                    db.collection("users/${email}/marks").document(mark.id.toString()).delete()
                    FirebaseStorage.getInstance().reference.child("images/${email}/${mark.id}").delete()
                    viewModel.loadMarks()
                    val action = ModifyFragmentDirections.actionModifyFragmentToMarkersFragment()
                    findNavController().navigate(action)
                }
                .setCancelable(false)
                .create()

            dialog.show()
        }

        mapIcon.setOnClickListener{
            viewModel.markerMap = position
            viewModel.markerZoom = true
            val action = ModifyFragmentDirections.actionModifyFragmentToMapFragment()
            findNavController().navigate(action)
        }
    }
}