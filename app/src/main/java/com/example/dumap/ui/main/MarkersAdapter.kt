package com.example.dumap.ui.main


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.dumap.R
import com.squareup.picasso.Picasso


class MarkersAdapter(private val marks: List<Marker>, private val listener: OnItemClickListener) : RecyclerView.Adapter<MarkersAdapter.ListOfChampsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListOfChampsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.marker,parent,false)

        return ListOfChampsViewHolder(view)
    }

    override fun onBindViewHolder(holderOfLists: ListOfChampsViewHolder, position: Int) {
        holderOfLists.bindData(marks[position])
    }

    override fun getItemCount(): Int {
        return marks.size
    }

    inner class ListOfChampsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var MarkerName: TextView
        private var MarkerDesc: TextView

        init {
            MarkerName = itemView.findViewById(R.id.mark_name)
            MarkerDesc = itemView.findViewById(R.id.mark_desc)
        }

        fun bindData(mark: Marker){
            MarkerName.text = mark.name
            MarkerDesc.text = mark.desc

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }
}
