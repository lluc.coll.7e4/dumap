package com.example.dumap.ui.views

import androidx.fragment.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.dumap.R
import com.example.dumap.ui.main.ViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth


class LoginFragment : Fragment(R.layout.login_fragment) {
    lateinit var username: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var register: TextView
    lateinit var login: Button

    private val viewModel: ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //val inflater = TransitionInflater.from(requireContext())
        //exitTransition = inflater.inflateTransition(R.transition.slide_left)
        //enterTransition = inflater.inflateTransition(R.transition.slide_right)


        FirebaseAuth.getInstance().
        signInWithEmailAndPassword("llukins02@gmail.com", "123456")
        username = view.findViewById(R.id.username)
        password = view.findViewById(R.id.password)
        register = view.findViewById(R.id.register)
        login = view.findViewById(R.id.login)

        register.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.registerFragment)
        }

        login.setOnClickListener{
            if(username.text.toString() != null && password.text.toString() != null){
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(username.text.toString(), password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        viewModel.email = emailLogged!!
                        viewModel.loadMarks()
                        Navigation.findNavController(view).navigate(R.id.mapFragment)
                    }
                    else{
                        password.error = "Error al login"
                        password.setText("")
                    }
                }
            }
            else{
                password.error = "Error al login"

            }
        }
    }
}
