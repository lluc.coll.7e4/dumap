package com.example.dumap.ui.views

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dumap.R
import com.example.dumap.ui.main.Marker
import com.example.dumap.ui.main.MarkersAdapter
import com.example.dumap.ui.main.ViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*

class MarkersFragment: Fragment(R.layout.markers_fragment), MarkersAdapter.OnItemClickListener {
    lateinit var searchBar: SearchView
    lateinit var recyclerView: RecyclerView
    lateinit var logout: Button
    lateinit var addButton: FloatingActionButton
    lateinit var fusedLoc: FusedLocationProviderClient

    private val viewModel: ViewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        searchBar = view.findViewById(R.id.searchBar)
        recyclerView = view.findViewById(R.id.recyclerView)
        logout = view.findViewById(R.id.logout)
        addButton = view.findViewById(R.id.newMarker)

        recyclerView.layoutManager = GridLayoutManager(requireContext(), 1)
        val docAdapter = MarkersAdapter(viewModel.getMarkers(), this)
        recyclerView.adapter = docAdapter

        Handler().postDelayed({
            val docAdapter = MarkersAdapter(viewModel.getMarkers(), this)
            recyclerView.adapter = docAdapter
        }, 1500)


        searchBar.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val docAdapter = MarkersAdapter(viewModel.search(newText!!), this@MarkersFragment)
                recyclerView.adapter = docAdapter
                return false
            }
        })


        logout.setOnClickListener{
            val email = viewModel.email
            val dialog = AlertDialog.Builder(context)
                .setTitle("Warning")
                .setMessage("Are you sure you want to logout from: "+email)
                .setNegativeButton("Cancel") { view, _ -> }
                .setPositiveButton("Continue") { view, _ ->
                    FirebaseAuth.getInstance().signOut()
                    viewModel.email = ""
                    val action = MarkersFragmentDirections.actionMarkersFragmentToLoginFragment()
                    findNavController().navigate(action)
                }
                .setCancelable(false)
                .create()

            dialog.show()

        }

        addButton.setOnClickListener{
            loadCordenades()
        }
    }

    override fun onItemClick(position: Int) {
        val action = MarkersFragmentDirections.actionMarkersFragmentToModifyFragment(position)
        findNavController().navigate(action)
    }

    fun loadCordenades(){
        fusedLoc = LocationServices.getFusedLocationProviderClient(context)
        if (ActivityCompat.checkSelfPermission(
                this.context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this.context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLoc.lastLocation.addOnSuccessListener {
            val action = MarkersFragmentDirections.actionMarkersFragmentToAddMarker(it.latitude.toFloat(), it.longitude.toFloat())
            findNavController().navigate(action)
        }
    }
}