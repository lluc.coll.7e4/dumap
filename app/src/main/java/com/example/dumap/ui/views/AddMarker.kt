package com.example.dumap.ui.views

import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.drawToBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dumap.R
import com.example.dumap.ui.main.Marker
import com.example.dumap.ui.main.ViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class AddMarker : Fragment(R.layout.add_marker) {
    lateinit var lat: TextInputEditText
    lateinit var lng: TextInputEditText
    lateinit var markerName: TextInputEditText
    lateinit var markerDesc: TextInputEditText
    lateinit var image: ImageView
    lateinit var addButton: Button

    var resultImg = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if (result.resultCode == RESULT_OK){
            result.data.let { if (it != null){
                //uriImg = convertoToUri(it.extras!!.get("data") as Bitmap)
                image.setImageBitmap(it.extras!!.get("data") as Bitmap)
            }
            }
        }
    }

    private val viewModel: ViewModel by activityViewModels()
    private val db = FirebaseFirestore.getInstance()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)




        lat = view.findViewById(R.id.lat)
        lng = view.findViewById(R.id.lng)
        markerName = view.findViewById(R.id.markerName)
        markerDesc = view.findViewById(R.id.markerDesc)
        image = view.findViewById(R.id.cameraImage)
        addButton = view.findViewById(R.id.add)

        lat.setText(arguments?.getFloat("lat").toString())
        lng.setText(arguments?.getFloat("lng").toString())


        addButton.setOnClickListener{
            if(markerName.text!!.length != 0){
                val marker = Marker(viewModel.getLastId(), markerName.text.toString(), " ", arguments?.getFloat("lat")!!, arguments?.getFloat("lng")!!)
                if(markerDesc.text!!.length != 0){
                    marker.desc = markerDesc.text.toString()
                }
                viewModel.markersList.add(marker)

                val email = viewModel.email
                db.collection("users/${email}/marks").document(marker.id.toString()).set(
                    hashMapOf(
                        "lat" to marker.lat,
                        "lng" to marker.lng,
                        "name" to marker.name,
                        "desc" to marker.desc,
                    )
                )

                FirebaseStorage.getInstance().getReference("images/${email}/${marker.id}").putFile(viewModel.toURI(context!!, image.drawToBitmap()))
                    .addOnSuccessListener {
                        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                    }


                val action = AddMarkerDirections.actionAddMarkerToMarkersFragment()
                findNavController().navigate(action)
            }
            else{
                Toast.makeText(requireContext(), "Complete at least the name!", Toast.LENGTH_SHORT).show()
            }

        }

        image.setOnClickListener{
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            try {
                resultImg.launch(takePictureIntent)
            } catch (e: ActivityNotFoundException) {
            }
        }
    }
}