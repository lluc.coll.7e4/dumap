package com.example.dumap.ui.main

import android.graphics.drawable.Drawable

data class Marker (var id: Int, var name: String, var desc: String, val lat: Float, val lng: Float)