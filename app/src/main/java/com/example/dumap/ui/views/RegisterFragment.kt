package com.example.dumap.ui.views

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.dumap.R
import com.example.dumap.ui.main.ViewModel
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth


class RegisterFragment: Fragment(R.layout.register_fragment) {
    lateinit var username: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var register: TextView
    lateinit var login: Button

    private val viewModel: ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //val inflater = TransitionInflater.from(requireContext())
        //exitTransition = inflater.inflateTransition(R.transition.slide_left)
        //enterTransition = inflater.inflateTransition(R.transition.slide_right)


        username = view.findViewById(R.id.username)
        password = view.findViewById(R.id.password)
        register = view.findViewById(R.id.register)
        login = view.findViewById(R.id.login)

        register.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.loginFragment)
        }

        login.setOnClickListener {
            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(username.text.toString(), password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        viewModel.email = emailLogged!!
                        viewModel.loadMarks()
                        Navigation.findNavController(view).navigate(R.id.mapFragment)
                        //goToHome(emailLogged!!)
                    }
                    else{
                        password.error = "Error al registrar l'usuari"
                        password.setText("")
                        Log.d("meh", "vaja")                    }
                }
        }
    }
}
